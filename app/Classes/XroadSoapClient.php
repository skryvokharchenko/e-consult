<?php

namespace App\Classes;

use SoapClient;
use SoapHeader;
use SoapVar;
use SoapServer;

class XroadSoapClient extends SoapClient
{

    function __construct($wsdl, $options)
    {
        parent::__construct($wsdl, $options);
        $this->server = new SoapServer($wsdl, $options);
    }

    function __doRequest($request, $location, $action, $version,  $one_way = NULL)
    {

        $request = str_replace('<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://arireg.x-road.eu/producer/" xmlns:ns2="http://x-road.eu/xsd/xroad.xsd">', '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://arireg.x-road.eu/producer/" xmlns:ns2="http://x-road.eu/xsd/xroad.xsd" xmlns:ns3="http://x-road.eu/xsd/identifiers">', $request);
        // parent call
        return parent::__doRequest($request, $location, $action, $version);
    }

}

