<?php

namespace App\Classes;

use Sk\SmartId\Client;
use Sk\SmartId\Exception\SmartIdException;
use Sk\SmartId\Api\Data\NationalIdentity;
use Sk\SmartId\Api\Data\AuthenticationHash;
use Sk\SmartId\Api\Data\CertificateLevelCode;

class SmartIDAuthClass
{
    const NATIONAL_IDENTITY = "EE";
    
    public function __construct()
    {
        
    }
    
    /**
     * @param string $personal_code
     * @return array
     *
     */
    public function authenticateSmartId($personal_code):array
    {
        $client = new Client();
        $client->setRelyingPartyUUID( config('estonianId.sid_service_uuid') )
            ->setRelyingPartyName( config('estonianId.sid_service_name') )
            ->setHostUrl( config('estonianId.sid_service_url') );
        
        // Consists of country and personal identity code
        $identity = new NationalIdentity( self::NATIONAL_IDENTITY, $personal_code );
        
        // For security reasons a new hash value must be created for each new authentication request
        $authenticationHash = AuthenticationHash::generate();
        
        $verificationCode = $authenticationHash->calculateVerificationCode();
        
        try{
            $authenticationResponse = $client->authentication()
                ->createAuthentication()
                ->withNationalIdentity( $identity ) 
                ->withAuthenticationHash( $authenticationHash )
                ->withCertificateLevel( CertificateLevelCode::QUALIFIED ) // Certificate level can either be "QUALIFIED" or "ADVANCED"
                ->authenticate();
            
            $return['status'] = 'Ok';
            $return['personal_code'] = $personal_code;
            $return['signedData'] = $authenticationResponse->signedData;
            return $return;
        }
        catch(UserRefusedException $e){
            $return['status'] = false;
            $return['message'] = trans('register.smart_error_user_refused');
            return $return;
        }
        catch(SessionTimeoutException $e){
            $return['status'] = false;
            $return['message'] = trans('register.smart_error_session_timeout');
            return $return;
        }
        catch (SmartIdException $e) {
            $return['status'] = false;
            $return['message'] = $e->getMessage();
            return $return;
        }
    }
}
