<?php

namespace App\Classes;

use Sk\Mid\DisplayTextFormat;
use Sk\Mid\Exception\DeliveryException;
use Sk\Mid\Exception\InvalidNationalIdentityNumberException;
use Sk\Mid\Exception\InvalidPhoneNumberException;
use Sk\Mid\Exception\InvalidUserConfigurationException;
use Sk\Mid\Exception\MidInternalErrorException;
use Sk\Mid\Exception\MidSessionNotFoundException;
use Sk\Mid\Exception\MidSessionTimeoutException;
use Sk\Mid\Exception\MissingOrInvalidParameterException;
use Sk\Mid\Exception\NotMidClientException;
use Sk\Mid\Exception\PhoneNotAvailableException;
use Sk\Mid\Exception\UnauthorizedException;
use Sk\Mid\Exception\UserCancellationException;
use Sk\Mid\Language\ENG;
use Sk\Mid\MobileIdAuthenticationHashToSign;
use Sk\Mid\MobileIdClient;
use Sk\Mid\Rest\Dao\Request\AuthenticationRequest;
use Sk\Mid\Util\MidInputUtil;

class MobileIDAuthClass
{
    public function __construct()
    {
        
    }
    
    /**
     * @param mixed[] $params
     * @return array
     *
     */
    public static function initiateMIDAuth(array $params):array
    {   
        try {
            $phoneNumber = MidInputUtil::getValidatedPhoneNumber($params['mid_phone_number']);
            $nationalIdentityNumber = MidInputUtil::getValidatedNationalIdentityNumber($params['mid_personal_code']);
        } catch (InvalidPhoneNumberException $e) {
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.phone_number_is_invalid');
            return $return;
        } catch (InvalidNationalIdentityNumberException $e) {
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.niid_is_invalid');
            return $return;
        }
        
        // create client with long-polling
        $client = MobileIdClient::newBuilder()
            ->withRelyingPartyUUID(config('estonianId.mid_service_uuid'))
            ->withRelyingPartyName(config('estonianId.mid_service_name'))
            ->withHostUrl(config('estonianId.mid_service_url'))
            ->withLongPollingTimeoutSeconds(60)
            ->withPollingSleepTimeoutSeconds(2)
            ->build();
        
        // generate hash & calculate verification code and display to user

        $authenticationHash = MobileIdAuthenticationHashToSign::generateRandomHashOfDefaultType();
        $verificationCode = $authenticationHash->calculateVerificationCode();
        
        $request = AuthenticationRequest::newBuilder()
            ->withPhoneNumber($phoneNumber)
            ->withNationalIdentityNumber($nationalIdentityNumber)
            ->withHashToSign($authenticationHash)
            ->withLanguage(ENG::asType())
            ->withDisplayText(config('estonianId.mid_auth_message'))
            ->withDisplayTextFormat(DisplayTextFormat::GSM7)
            ->build();

        // send request to user's phone and catch possible errors

        try {
            $response = $client->getMobileIdConnector()->initAuthentication($request);
        } catch (NotMidClientException $e) {
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.not_mid_client');
            return $return;
        } catch (UnauthorizedException $e) {
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.invalid_mid_credentials');
            return $return;
        } catch (MissingOrInvalidParameterException $e) {
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.niid_is_invalid');
            return $return;
        } catch (MidInternalErrorException $e) {
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.mid_internal_error');
            return $return;
        }
        $return = array();
        $return['challengeId'] = $verificationCode;
        $sessCode = $response->getSessionID();
        $return['status'] = true;
        $return['EidMidClient'] = $client;
        $return['MIDAuthHash'] = $authenticationHash;
        $return['MIDSessCode'] = $sessCode;

        return $return;
    }
    
    /**
     * @param string $sessCode
     * @param MobileIdAuthenticationHashToSign $authHash
     * @param MobileIdClient $eidMidClient
     * @return array
     *
     */
    
    public static function checkMIDAuth(string $sessCode, MobileIdAuthenticationHashToSign $authHash, MobileIdClient $eidMidClient): array
    {
        if (!$sessCode || $sessCode === null) {
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.wrong_session_id');
            return $return;
        }
        
        // step #7 - keep polling for session status until we have a final status from phone
        
        try {
            $finalSessionStatus = $eidMidClient->getSessionStatusPoller()->fetchFinalSessionStatus($sessCode);
        } catch (UserCancellationException $e) {
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.operation_canceled');
            return $return;
        } catch (MidSessionTimeoutException $e) {
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.no_pin_in_phone');
            return $return;
        } catch (PhoneNotAvailableException $e) {
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.unable_to_reach_phone');
            return $return;
        } catch (DeliveryException $e) {
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.communication_error_phone');
            return $return;
        } catch (InvalidUserConfigurationException $e) {
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.sim_card_conf_differs_from_provider');
            return $return;
        } catch (MidSessionNotFoundException | MissingOrInvalidParameterException | UnauthorizedException $e) {
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.client_side_error_with_mid') . $e->getCode();
            return $return;
        } catch (NotMidClientException $e) {
            // if user is not MID client then this exception is thrown and caught already during first request
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.not_mid_client_or_revoked_cert');
            return $return;
        } catch (MidInternalErrorException $internalError) {
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.something_went_wrong');
            return $return;
        }
        
        // step #8 - parse authenticated person out of the response and get it validated

        try {
            $authenticatedPerson = $eidMidClient
                ->createMobileIdAuthentication($finalSessionStatus, $authHash)
                ->getValidatedAuthenticationResult()
                ->getAuthenticationIdentity();
        } catch (UserCancellationException $e) {
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.operation_canceled');
            return $return;
        } catch (MidSessionTimeoutException $e) {
            $return['status'] = false;
            $return['message'] =  trans('register.mid_errors.no_pin_in_phone');
            return $return;
        } catch (PhoneNotAvailableException $e) {
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.unable_to_reach_phone');
            return $return;
        } catch (DeliveryException $e) {
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.communication_error_phone');
            return $return;
        } catch (InvalidUserConfigurationException $e) {
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.sim_card_conf_differs_from_provider');
            return $return;
        } catch (MidSessionNotFoundException | MissingOrInvalidParameterException | UnauthorizedException $e) {
            $return['status'] = false;
            $return['message'] = "Client side error with mobile-ID integration. Error code:" . $e->getCode();
            return $return;
        } catch (NotMidClientException $e) {
            // if user is not MID client then this exception is thrown and caught already during first request
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.client_side_error_with_mid');
            return $return;
        } catch (MidInternalErrorException $internalError) {
            $return['status'] = false;
            $return['message'] = trans('register.mid_errors.something_went_wrong');
            return $return;
        }
        
        # step #9 - read out authenticated person details
        $return['status'] = true;
        $return['finished'] = true;
        $return['message'] = "All good";
        $return["firstName"] = $authenticatedPerson->getGivenName();
        $return["lastName"] = $authenticatedPerson->getSurName();
        $return["socialSecurityNumber"] = $authenticatedPerson->getIdentityCode();
        $return["country"] = $authenticatedPerson->getCountry();
        $return["sessionNumber"] = $sessCode;
        return $return;
    }
}

