<?php

namespace App\Classes;

use App\Application;
use \GuzzleHttp\Client;
use SoapClient;
use App\Classes\XroadSoapClient;
use SoapHeader;
use SoapVar;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use App\Classes\XroadXMLClass;

class RegistrationClass
{
    //TODO dev environment, that we will change to production
    const ARIREG_WSDL_URL = "https://xdev-v6.almic.ee/cgi-bin/consumer_proxy/";

    const ARIREG_LOCAL_CERT_PATH = '/var/www/e-consult/public/econsult.crt';

    const ARIREG_NAME_CHECK_SERVICE = "nimeSobivus_v1";

    const ARIREG_ENTRY_CREATION_SERVICE = 'evapiLooKanne_v1';

    const ARIREG_ENTRY_CONFIRMATION_SERVICE = 'evapiKinnitaKanne_v1';

    const ARIREG_ENTRY_DELETION_SERVICE = 'evapiKustutaKanne_v1';

    const XROAD_NAMESPACE = 'http://x-road.eu/xsd/xroad.xsd';

    const XROAD_IDENTIFIERS_NAME_SPACE = 'http://x-road.eu/xsd/identifiers';

    const ARIREG_NAME_SPACE = 'http://arireg.x-road.eu/producer/';

    const XROAD_PROTOCOL_VERSION = '4.0';

    //For return when pasing responce
    const NO_MATCHES_FOUND = 'Ok';

    const EXACT_MATCH_TYPE = 'T';

    const PARTIAL_MATCH_TYPE = 'S';

    const OUTPUT_LANGUAGE = 'eng';
    /**
     * @var SoapWrapper
     */

    protected $xroadXMLClass;

    public function __construct()
    {
        $this->xroadXMLClass = new XroadXMLClass();
    }

    //Build name check request
    public function checkNameRequest($params = false)
    {
        if(!empty($params)){
            try{

                $soap_body_xml = $this->xroadXMLClass->prepareXroadCheckNameRequestBody([
                    'company_name' => $params['company_name'],
                    'precision' => 2
                ]);
                $soapBody = new SoapVar($soap_body_xml, XSD_ANYXML);

                $response = $this->makeSoapRequest([
                    'arireg_service' => self::ARIREG_NAME_CHECK_SERVICE,
                    'soap_body' => $soapBody
                ]);

                $procesed_data = $this->processXroadCheckNameResponse($response);


                return $procesed_data;

            }catch(SoapFault $e){

            }
        }
        return false;
    }

    public function createEntryRequest($params)
    {
        if(!empty($params)){
            try{
                $data_from_db = Application::where('session_id', '=', $params['session_id'])->first();

                if(!empty($data_from_db)){

                    $request_array = [
                        'partner_auth' => $data_from_db->login_data,
                        'third_party_system_application_id' => '14355965',
                        'performer_fullname' => 'MARY ÄNN',
                        'performer_personal_id' => '60001019906',
                        'state_fee_reference_numbers' => '200',
                        'company_name' => $data_from_db->company_name
                    ];

                    if(!empty($data_from_db->company_address_estonias)){
                        $request_array['company_location'] = [
                            'postindeks' => '10114',
                            'country' => 'EE',
                            'norm_addr_text' => 'Tallin, Harjumaakond, Kaupmehe 7-120',
                            'EHAK' => '0296',
                            'adr_id' => '14355965'
                        ];
                    }

                    if(!empty($data_from_db->user_email)){
                        $request_array['persons_array'] = [
                            'person_type' => 'F',
                            'person_role' => 'JUHL',
                            'first_name' => 'MARY',
                            'last_name' => 'ÄNN',
                            'est_personal_id_code' => '60001019906',
                            'residence' => 'United Kingdom of Greate Britain and Northern Ireland, London 10 Greate Istern street',
                            'personal_email' => 'someemail@gmail.com',
                            'contact_info' => 'someemail@gmail.com',
                            'manner_of_impersion' => 'founder',
                            'personal_adress' => [
                                'postindex' => '12345',
                                'contry' => 'UK',
                                'norm_addr_text' => 'United Kingdom of Greate Britain and Northern Ireland, London 10 Greate Istern street',
                                'adr_id' => '1232435465'
                            ]
                        ];
                    }

                    if(!empty($data_from_db->share_capital)){
                        $request_array['capital'] = [
                            'capital' => $data_from_db->share_capital,
                            'currency' => 'EUR'
                        ];
                    }

                    if(!empty($data_from_db->company_activity)){
                        $request_array['main_activity'] = [
                            'EMTAK' => '62091',
                            'version' => '2008',
                            'activity' => 'Other information technology and computer service activity'
                        ];
                    }

                    if(!empty($data_from_db->company_email)){
                        $request_array['means_of_communication'] = [
                            'species' => 'email',
                            'number_with_area_code' => $data_from_db->company_email
                        ];
                    }

                    $soap_body_xml = $this->xroadXMLClass->prepareXroadCreateEntryRequestBody($request_array);
                    $soapBody = new SoapVar($soap_body_xml, XSD_ANYXML);

                    $response = $this->makeSoapRequestDebug([
                        'arireg_service' => self::ARIREG_ENTRY_CREATION_SERVICE,
                        'soap_body' => $soapBody
                    ]);

                    $procesed_data = $this->processXroadEntryCreationServiceResponse($response);

                    return $procesed_data;
                }

            } catch (SoapFault $ex) {

            }
        }
        return false;
    }

    public function confirmEntryRequest($params)
    {
        if(!empty($params)){
            $soap_body_xml = $this->xroadXMLClass->prepareXroadConfirmEntryRequestBody([
                'partner_auth' => '60001019906',
                'entry_id' => '123456',
                'signed_document_ref' => 'data.bin'
            ]);
            $soapBody = new SoapVar($soap_body_xml, XSD_ANYXML);

            $response = $this->makeSoapRequestDebug([
                'arireg_service' => self::ARIREG_ENTRY_CONFIRMATION_SERVICE,
                'soap_body' => $soapBody,
                'with_detachment' => true
            ]);

            $processed_data = $this->processXroadEntryConfirmationServiceResponce($response);

            return $processed_data;
        }
        return false;
    }

    public function deleteEnrtyRequest($params)
    {
        if(!empty($params)){
            $soap_body_xml = $this->xroadXMLClass->prepareXroadDeleteEntryRequestBody();
            $soapBody = new SoapVar($soap_body_xml, XSD_ANYXML);

            $response = $this->makeSoapRequest([
                'arireg_service' => self::ARIREG_ENTRY_DELETION_SERVICE,
                'soap_body' => $soapBody
            ]);

            $processed_data = $this->processXroadEntryDeletionServiceResponce($response);

            return $processed_data;
        }
        return false;
    }

    protected function makeSoapRequest($params)
    {
        if(!empty($params)){
            $soapClientOptions = $this->createSoapClientOptions($params['arireg_service']);

            $client = new XroadSoapClient(NULL, $soapClientOptions);

            $xroad_client_var_xml = $this->xroadXMLClass->prepareXroadSoapClientHeader();
            $xroad_client_var = new SoapVar($xroad_client_var_xml, XSD_ANYXML);

            $service_var_xml = $this->xroadXMLClass->prepareXroadSoapSystemtHeader($params['arireg_service']);
            $service_var = new SoapVar($service_var_xml, XSD_ANYXML);

            $message_uiid = uniqid();

            $soapBody = $params['soap_body'];

            $service = new SoapHeader(self::XROAD_NAMESPACE, 'RequestParams', $service_var);
            $xroad_client = new SoapHeader(self::XROAD_NAMESPACE, 'RequestParams', $xroad_client_var);
            $id = new SoapHeader(self::XROAD_NAMESPACE, 'id',$message_uiid);
            $protocol = new SoapHeader(self::XROAD_NAMESPACE, 'protocolVersion', self::XROAD_PROTOCOL_VERSION);

            $client->__SetSoapHeaders(array($xroad_client, $service, $id, $protocol));
            $response = $client->__soapCall($params['arireg_service'], array($soapBody));

            return $response;
        }
    }
    //TO DO: Remove later. Just for debug.
    //When making $client->__getLastRequest(), $client->__getLastResponse() it changes the value in $response variable, and get not array but Soap body
    protected function makeSoapRequestDebug($params)
    {
        if(!empty($params)){
            if(!empty($params['with_detachment'])){
                $soapClientOptions = $this->createSoapClientOptionsDetachment($params['arireg_service']);
                var_dump('TEST');
            }else{
                $soapClientOptions = $this->createSoapClientOptions($params['arireg_service']);
            }

            $client = new XroadSoapClient(NULL, $soapClientOptions);

            $xroad_client_var_xml = $this->xroadXMLClass->prepareXroadSoapClientHeader();
            $xroad_client_var = new SoapVar($xroad_client_var_xml, XSD_ANYXML);

            $service_var_xml = $this->xroadXMLClass->prepareXroadSoapSystemtHeader($params['arireg_service']);
            $service_var = new SoapVar($service_var_xml, XSD_ANYXML);

            $message_uiid = uniqid();

            $soapBody = $params['soap_body'];

            $service = new SoapHeader(self::XROAD_NAMESPACE, 'RequestParams', $service_var);
            $xroad_client = new SoapHeader(self::XROAD_NAMESPACE, 'RequestParams', $xroad_client_var);
            $id = new SoapHeader(self::XROAD_NAMESPACE, 'id',$message_uiid);
            $protocol = new SoapHeader(self::XROAD_NAMESPACE, 'protocolVersion', self::XROAD_PROTOCOL_VERSION);

            $client->__SetSoapHeaders(array($xroad_client, $service, $id, $protocol));
            $response = $client->__soapCall($params['arireg_service'], array($soapBody));
            //Debug methods
            var_dump($client->__getLastRequest());
            var_dump($client->__getLastRequestHeaders());
            var_dump($client->__getLastResponse());
            var_dump($client->__getLastResponseHeaders());

            return $response;
        }
    }
    //The simple client options
    protected function createSoapClientOptions($service)
    {
        $opts = array(
                    'ssl'=>array(
                        'verify_peer'=>false,
                        'verify_peer_name'=>false,
                        'allow_self_signed' => true,
                        'local_cert' => self::ARIREG_LOCAL_CERT_PATH
                    ),
                    'https' => array(
                        'curl_verify_ssl_peer'  => false,
                        'curl_verify_ssl_host'  => false,
                        'user_agent' => 'PHPSoapClient',
                        'allow_self_signed' => true
                    )
                );

        $context = stream_context_create($opts);

        $soapClientOptions = array(
            'trace' => 1,
            'location'=>self::ARIREG_WSDL_URL,
            'uri' => self::ARIREG_NAME_SPACE,
            'exceptions' => 0,
            'cache_wsdl'=>0,
            'keep_alive' => false,
            'verifypeer' => true,
            'verifyhost' => true,
            'stream_context' => $context,
            'classmap' => array($service => $service),
            'use' => SOAP_LITERAL
        );

        return  $soapClientOptions;
    }
    //Client options to deal with attachments. Headers are different for content types of multypart fields
    // --MIME boundaries
    protected function createSoapClientOptionsDetachment($service)
    {
        //SSL options are still the same. We need to authenticate, and send certificate
        $opts = array(
                    'ssl'=>array(
                        'verify_peer'=>false,
                        'verify_peer_name'=>false,
                        'allow_self_signed' => true,
                        'local_cert' => self::ARIREG_LOCAL_CERT_PATH
                    ),
                    'https' => array(
                        'curl_verify_ssl_peer'  => false,
                        'curl_verify_ssl_host'  => false,
                        'user_agent' => 'PHPSoapClient',
                        'allow_self_signed' => true
                    )
                );
        $context = stream_context_create($opts);
        var_dump($context);

        $soapClientOptions = array(
            'header' => 'Content-Type: application/octet-stream; name=data.bin\r\n Content-Transfer-Encoding: base64\r\n Content-ID: <data.bin>\r\n Content-Disposition: attachment; name="data.bin"; filename="data.bin"\r\n',
            'trace' => 1,
            'location'=>self::ARIREG_WSDL_URL,
            'uri' => self::ARIREG_NAME_SPACE,
            'exceptions' => 0,
            'cache_wsdl'=>0,
            'keep_alive' => false,
            'verifypeer' => true,
            'verifyhost' => true,
            'stream_context' => $context,
            'classmap' => array($service => $service),
            'use' => SOAP_LITERAL
        );



        return  $soapClientOptions;
    }

    protected function processXroadCheckNameResponse($response)
    {
        if(!empty($response)){
            foreach($response['keha'] as $response_body){
                if(!empty($response_body->item)){
                    $matches_found = array();

                    foreach($response_body->item as $item){
                        $matches_found[] = $item->tekst;
                    }

                    if(!empty($matches_found)){
                        return $matches_found;
                    }else{
                        return self::NO_MATCHES_FOUND;
                    }
                }else{
                    return self::NO_MATCHES_FOUND;
                }
            }
        }
    }

    protected function processXroadEntryCreationServiceResponse($response)
    {

    }

    protected function processXroadEntryDeletionServiceResponce($params)
    {

    }

    protected function processXroadEntryConfirmationServiceResponce($params)
    {

    }

}
