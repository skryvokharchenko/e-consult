<?php

namespace App\Classes;

class XroadXMLClass
{
    const ARIREG_INSTANCE = 'ee-dev';

    //Client header settings
    const ARIREG_MEMBER_CLASS = 'COM';

    const ARIREG_MEMBER_CODE = '14355965';

    const ARIREG_MEMBER_SUBSYTEM_CODE = 'emta';

    //Service header settings
    const ARIREG_SERVICE_SUBSYSTEM_CODE = 'arireg';

    const ARIREG_SERVICE_MEMBER_CLASS = 'GOV';

    const ARIREG_SERVICE_MEMBER_CODE = '70000310';

    const ARIREG_SERVICE_VERSION = 'v1';

    //For return when pasing responce
    const NO_MATCHES_FOUND = 'Ok';

    const EXACT_MATCH_TYPE = 'T';

    const PARTIAL_MATCH_TYPE = 'S';

    const OUTPUT_LANGUAGE = 'eng';


    public function __construct()
    {

    }

    public function prepareXroadSoapClientHeader()
    {
        return $xml = '<ns2:client ns3:objectType="SUBSYSTEM">'
                            .'<ns3:xRoadInstance>'.self::ARIREG_INSTANCE.'</ns3:xRoadInstance>'
                            .'<ns3:memberClass>'.self::ARIREG_MEMBER_CLASS.'</ns3:memberClass>'
                            .'<ns3:memberCode>' .self::ARIREG_MEMBER_CODE.'</ns3:memberCode>'
                            .'<ns3:subsystemCode>'.self::ARIREG_MEMBER_SUBSYTEM_CODE.'</ns3:subsystemCode>'
                        .'</ns2:client>';

    }

    public function prepareXroadSoapSystemtHeader($arireg_service)
    {
         return $xml = '<ns2:service ns3:objectType="SERVICE">'
                            .'<ns3:xRoadInstance>'.self::ARIREG_INSTANCE.'</ns3:xRoadInstance>'
                            .'<ns3:memberClass>'.self::ARIREG_SERVICE_MEMBER_CLASS .'</ns3:memberClass>'
                            .'<ns3:memberCode>'.self::ARIREG_SERVICE_MEMBER_CODE.'</ns3:memberCode>'
                            .'<ns3:subsystemCode>'.self::ARIREG_SERVICE_SUBSYSTEM_CODE.'</ns3:subsystemCode>'
                            .'<ns3:serviceCode>'.$arireg_service.'</ns3:serviceCode>'
                            .'<ns3:serviceVersion>'.self::ARIREG_SERVICE_VERSION.'</ns3:serviceVersion>'
                       .'</ns2:service>';

    }

    public function prepareXroadCheckNameRequestBody($params)
    {
        $xml = '<ns1:keha>'
                    .'<ns1:nimi>'.$params['company_name'].'</ns1:nimi>'
                    .'<ns1:tapsusklass>'.$params['precision'].'</ns1:tapsusklass>'
                    .'<ns1:keel>'.self::OUTPUT_LANGUAGE.'</ns1:keel>'
                .'</ns1:keha>';

        return $xml;
    }

    public function prepareXroadCreateEntryRequestBody($params)
    {
        $xml = '<ns1:keha>'
                    .'<ns1:partner_auth>'.$params['partner_auth'].'</ns1:partner_auth>'               //Auth information
                    .'<ns1:kande_id>'.$params['third_party_system_application_id'].'</ns1:kande_id>'
                    .'<ns1:esitaja_nimi>'.$params['performer_fullname'].'</ns1:esitaja_nimi>'
                    .'<ns1:esitaja_kood>'.$params['performer_personal_id'].'</ns1:esitaja_kood>'
                    .'<ns1:viitenumber_loiv>'.$params['state_fee_reference_numbers'].'</ns1:viitenumber_loiv>'
                    .'<ns1:kande_sisu>'                                                             //Entry documents
                        .'<ns1:uus_arinimi>'.$params['company_name'].'</ns1:uus_arinimi>';           //Company name

        $xml_arr = array();

        if(!empty($params['company_location'])) $xml_arr[] = $this->getXMLLocation($params['company_location'], 'asukoht');
        if(!empty($params['main_activity'])) $xml_arr[] = $this->getXMLActivity($params['main_activity']);
        if(!empty($params['capital'])) $xml_arr[] = $this->getXMLCapital($params['capital']);
        if(!empty($params['persons_array'])) $xml_arr[] = $this->getXMLPersonsArray($params['persons_array']);
        if(!empty($params['means_of_communication'])) $xml_arr[] = $this->getXMLMeansOfCommunication($params['means_of_communication']);

        if(!empty($xml_arr)){
            foreach($xml_arr as $element){
                $xml = $xml . $element;
            }
        }

        $xml = $xml . '</ns1:kande_sisu></ns1:keha>';

        return $xml;
    }

    public function prepareXroadConfirmEntryRequestBody($params)
    {
        $xml = '<ns1:keha>'
                    .'<ns1:partner_auth>'.$params['partner_auth'].'</ns1:partner_auth>'
                    .'<ns1:ekanded_id>'.$params['entry_id'].'</ns1:ekanded_id>'
                    .'<ns1:viide_manusele>cid:'.$params['signed_document_ref'].'</ns1:viide_manusele>'
                .'</ns1:keha>';

        return $xml;
    }

    public function prepareXroadDeleteEntryRequestBody($params)
    {
        $xml = '<ns1:keha>'
                    .'<ns1:partner_auth>'.$params['partner_auth'].'</ns1:partner>'
                    .'<ns1:ekanded_id>'.$params['entry_id'].'</ns1ekanded_id>'
                .'</ns1:keha>';

        return $xml;
    }

    protected function getXMLLocation($params, $opening_tag_name)
    {
        $xml_location = '<ns1:'.$opening_tag_name.'>';

        if(!empty($params['EHAK']))  $xml_location =  $xml_location . '<ns1:ehak>'.$params['EHAK'].'</ns1:ehak>';
        if(!empty($params['country']))  $xml_location =  $xml_location . '<ns1:riik>'.$params['country'].'</ns1:riik>';
        if(!empty($params['postindeks']))  $xml_location =  $xml_location . '<ns1:postiindeks>'.$params['postindeks'].'</ns1:postiindeks>';
        if(!empty($params['adob_id']))  $xml_location =  $xml_location . '<ns1:adob_id>'.$params['adob_id'].'</ns1:adob_id>';
        if(!empty($params['adr_id']))  $xml_location =  $xml_location . '<ns1:adr_id>'.$params['adr_id'].'</ns1:adr_id>';
        if(!empty($params['ads_oid']))  $xml_location =  $xml_location . '<ns1:ads_oid>'.$params['ads_oid'].'</ns1:ads_oid>';
        if(!empty($params['code_addr']))  $xml_location =  $xml_location . '<ns1:koodaadress>'.$params['code_addr'].'</ns1:koodaadress>';
        if(!empty($params['norm_addr_text']))  $xml_location =  $xml_location . '<ns1:taisaadress>'.$params['norm_addr_text'].'</ns1:taisaadress>';
        if(!empty($params['norm_addr_text_attach']))  $xml_location =  $xml_location . '<ns1:taisaadress_tapsustus>'.$params['norm_addr_text_attach'].'</ns1:taisaadress_tapsustus>';

        $xml_location = $xml_location . '</ns1:'.$opening_tag_name.'>';

        return $xml_location;
    }

    protected function getXMLActivity($params)
    {
        $xml_activity = '<ns1:pohitegevusala>';

        if(!empty($params['EMTAK'])) $xml_activity = $xml_activity . '<ns1:emtak_kood>'.$params['EMTAK'].'</ns1:emtak_kood>';
        if(!empty($params['version'])) $xml_activity = $xml_activity . '<ns1:versioon>'.$params['version'].'</ns1:versioon>';
        if(!empty($params['activity'])) $xml_activity = $xml_activity . '<ns1:nimetus>'.$params['activity'].'</ns1:nimetus>';

        $xml_activity = $xml_activity . '</ns1:pohitegevusala>';

        return $xml_activity;
    }

    protected function getXMLCapital($params)
    {
        $xml_capital = '<ns1:kapital>';

        if(!empty($params['capital'])) $xml_capital = $xml_capital . '<ns1:suurus>'.$params['capital'].'</ns1:suurus>';
        if(!empty($params['currency'])) $xml_capital = $xml_capital . '<ns1:valuuta>'.$params['currency'].'</ns1:valuuta>';

        $xml_capital = $xml_capital . '</ns1:kapital>';

        return $xml_capital;
    }

    protected function getXMLPersonsArray($params)
    {
        $xml_persons = '<ns1:isikud>';
        //Type of person: F - natural, J - legal (NB: considered self-employed)
        if(!empty($params['person_type'])) $xml_persons = $xml_persons . '<ns1:liik>'.$params['person_type'].'</ns1:liik>';
        //Role of the person: N - member of the supervisory board, JUHL - member of the management board, PROK - procurator, etc.
        if(!empty($params['person_role'])) $xml_persons = $xml_persons . '<ns1:roll>'.$params['person_role'].'</ns1:roll>';
        if(!empty($params['contribution_amount'])) $xml_persons = $xml_persons . '<ns1:sissemaksu_summa>'.$params['contribution_amount'].'</ns1:sissemaksu_summa>';
        if(!empty($params['contribution_currency'])) $xml_persons = $xml_persons . '<ns1:sissemaksu_valuuta>'.$params['contribution_currency'].'</ns1:sissemaksu_valuuta>';
        if(!empty($params['first_name'])) $xml_persons = $xml_persons . '<ns1:eesnimi>'.$params['first_name'].'</ns1:eesnimi>';
        if(!empty($params['last_name'])) $xml_persons = $xml_persons . '<ns1:nimi>'.$params['last_name'].'</ns1:nimi>';
        if(!empty($params['est_personal_id_code'])) $xml_persons = $xml_persons . '<ns1:kood>'.$params['est_personal_id_code'].'</ns1:kood>';
        if(!empty($params['residence'])) $xml_persons = $xml_persons . '<ns1:residentsus>'.$params['residence'].'</ns1:residentsus>';
        if(!empty($params['personal_email'])) $xml_persons = $xml_persons . '<ns1:email>'.$params['personal_email'].'</ns1:email>';
        if(!empty($params['contact_info'])) $xml_persons = $xml_persons . '<ns1:kontakt>'.$params['contact_info'].'</ns1:kontakt>';
        if(!empty($params['manner_of_impersion'])) $xml_persons = $xml_persons . '<ns1:kontrolli_teostamise_viis>'.$params['manner_of_impersion'].'</ns1:kontrolli_teostamise_viis>';

        if(!empty($params['personal_adress'])){
            $personal_adress = $this->getXMLLocation($params['personal_adress'], 'aadress');
            $xml_persons = $xml_persons . $personal_adress;
        }

        $xml_persons = $xml_persons . '</ns1:isikud>';

        return $xml_persons;
    }

    protected function getXMLMeansOfCommunication($params)
    {
        $xml_means_of_communication = '<ns1:sidevahendid>';

        if(!empty($params['species'])) $xml_means_of_communication = $xml_means_of_communication . '<ns1:liik>'.$params['species'].'</ns1:liik>';
        if(!empty($params['number_with_area_code'])) $xml_means_of_communication = $xml_means_of_communication . '<ns1:sisu>'.$params['number_with_area_code'].'</ns1:sisu>';

        $xml_means_of_communication = $xml_means_of_communication . '</ns1:sidevahendid>';

        return $xml_means_of_communication;
    }
}

