<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProceedPaymentRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'terms_of_service' => 'required|string',
            'privacy_policy' => 'required|string',
            'news_letter_subscribe' => 'string'
        ];
    }
}
