<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class CheckNameRequest extends FormRequest
{
    public function rules()
    {
        return [
            'company_name'    =>'required|string'
        ];
    }
}
