<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class SmartIdCheckRequest extends FormRequest
{
    public function rules()
    {
        return [
            'personal_code'   => 'required|string'
        ];
    }
}
