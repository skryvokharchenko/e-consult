<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;


class MobileIdCheckRequest extends FormRequest
{
    public function rules()
    {
        return [
            'mid_phone_number'    => 'required|string',
            'mid_personal_code'   => 'required|integer'
        ];
    }
}
