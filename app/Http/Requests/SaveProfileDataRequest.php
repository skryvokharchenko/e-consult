<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveProfileDataRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'     => 'required|string',
            'last_name'      => 'required|string',
            'personal_id'    => 'required|string',
            'user_email'     => 'required|string|email',
            'phone_number'   => 'required|string',
            'residence'      => 'required|string',
            'citizenship'    => 'required|string',
            'city'           => 'required|string',
            'street_address' => 'required|string',
            'state_province' => 'required|string',
            'postcode'       => 'required|string'
        ];
    }
}
