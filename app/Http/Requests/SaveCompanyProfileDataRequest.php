<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveCompanyProfileDataRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_code'             => 'required|string',
            'company_activity'         => 'required|string',
            'share_capital'            => 'required|string',
            'company_email'            => 'required|string|email',
            'company_phone_number'     => 'required|string',
            'subscribe_to'             => 'string',
            'company_address_estonias' => 'required|string',
        ];
    }
}
