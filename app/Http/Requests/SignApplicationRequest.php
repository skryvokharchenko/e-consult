<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SignApplicationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'digital_signature' => 'required|string'
        ];
    }
}
