<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CheckNameRequest;
use App\Http\Requests\SaveProfileDataRequest;
use App\Http\Requests\SaveCompanyProfileDataRequest;
use App\Http\Requests\SubmitApplicationRequest;
use App\Http\Requests\SignApplicationRequest;
use App\Http\Requests\ProceedPaymentRequest;
use App\Http\Requests\MobileIdCheckRequest;
use App\Http\Requests\SmartIdCheckRequest;

use App\Http\Controllers\Controller;
use App\Logger;
use App\Application;
use App\Classes\RegistrationClass;
use App\Classes\MobileIDAuthClass;
use App\Classes\SmartIDAuthClass;

use Session;

class RegistrationController extends Controller
{
    //steps for logger and output
    const REGISTER_NAME_STEP   = 1;
    const APPLICATION_STEP     = 2;
    const USER_PROFILE_STEP    = 3;
    const COMPANY_PROFILE_STEP = 4;
    const REVIEW_STEP          = 5;
    const DIGITAL_STEP         = 6;
    const PAYMENT_STEP         = 7;
    const FINAL_STEP           = 8;

    //classes instances
    protected $soapRequest;

    protected $mobileAuth;

    protected $smartAuth;

    public function __construct()
    {
        $this->soapRequest = new RegistrationClass();
        $this->mobileAuth = new MobileIDAuthClass();
        $this->smartAuth = new SmartIDAuthClass();
    }
    public function register()
    {
        $steps_array = $this->getStepsArray(self::REGISTER_NAME_STEP);

        return view('register.index', compact('steps_array'));
    }

    public function checkName(CheckNameRequest $request)
    {
            $inputs = $request->validated();

            $company_name = $inputs['company_name'];

            $session_id = session()->getId();

            if(!empty($session_id)){
                if(config('register.production_version') == true){
                    $soap_request_result = $this->soapRequest->checkNameRequest([
                        'company_name' => $company_name
                    ]);
                }

                $log_entry = $this->makeLogEntry([
                    'session_id' => $session_id,
                    'step_ended' => self::REGISTER_NAME_STEP
                ]);

                session(['application_data' => array()]);

                $this->addToSessionApplicationData([
                    'company_name' => $company_name
                ]);

                if(config('register.production_version') == true ){
                    return json_encode($soap_request_result);
                }
                return json_encode('Ok');
            }
    }

    public function application()
    {
        $steps_array = $this->getStepsArray(self::APPLICATION_STEP);

        return view('register.application', compact('steps_array'));

    }

    public function loginViaMID(MobileIdCheckRequest $request)
    {
        $inputs = $request->validated();

        if(!empty($inputs)){
             $session_id = session()->getId();

            if(!empty($session_id)){
                $log_entry = $this->makeLogEntry([
                    'session_id' => $session_id,
                    'step_ended' => self::APPLICATION_STEP
                ]);
            }

             if(empty($log_entry)){
                 return json_encode(['status' => 'session_fault']);
             }

             $mid_auth_response = $this->mobileAuth::initiateMIDAuth($inputs);

             if(!empty($mid_auth_response['status'])){
                $response = $this->mobileAuth::checkMIDAuth(
                     $mid_auth_response['MIDSessCode'],
                     $mid_auth_response['MIDAuthHash'],
                     $mid_auth_response['EidMidClient']
                );
             }else{
                return json_encode(['status' => 'auth_error', 'message' => $response['message']]);
             }

             if(!empty($response['status'])){

                $login_data = $response['socialSecurityNumber'];

                $application_data = session('application_data');

                $this->addOrCreateToApplicationTable([
                    'session_id' => $session_id,
                    'company_name' => $application_data['company_name'],
                    'login_data' => $login_data
                ]);

                return json_encode(['status' => 'Ok']);
             }else{
                 return json_encode(['status' => 'phone_error', 'message' => $response['message']]);
             }
        }

    }

    public function loginViaSmartID(SmartIdCheckRequest $request)
    {
        $inputs = $request->validated();
        if(!empty($inputs)){
            $session_id = session()->getId();

            if(!empty($session_id)){
                $log_entry = $this->makeLogEntry([
                    'session_id' => $session_id,
                    'step_ended' => self::APPLICATION_STEP
                ]);
            }

            if(empty($log_entry)){
                return json_encode(['status' => 'session_fault']);
            }

            $response = $this->smartAuth->authenticateSmartId($inputs['personal_code']);

            if(!empty($response['status'])){
                $login_data = $response['personal_code'];

                $application_data = session('application_data');

                $this->addOrCreateToApplicationTable([
                    'session_id' => $session_id,
                    'company_name' => $application_data['company_name'],
                    'login_data' => $login_data
                ]);

                return json_encode(['status' => 'Ok']);
            }else{
                return json_encode(['status' => 'personal_id_error', 'message' => $response['message']]);
            }
        }
    }

    public function yourProfile()
    {
        $steps_array = $this->getStepsArray(self::USER_PROFILE_STEP);

        return view('register.profile', compact('steps_array'));
    }

    public function saveUserProfileData(SaveProfileDataRequest $request)
    {
        $inputs = $request->validated();

        if(!empty($inputs)){

            $session_id = session()->getId();

            if(!empty($session_id)){
                $log_entry = $this->makeLogEntry([
                    'session_id' => $session_id,
                    'step_ended' => self::USER_PROFILE_STEP
                ]);
            }

            if(empty($log_entry)){
                return json_encode(false);
            }

            $this->addToSessionApplicationData($inputs);

            $inputs['session_id'] = $session_id;

            $this->addOrCreateToApplicationTable($inputs);

            return json_encode(true);
        }
    }

    public function companyProfile()
    {
        $steps_array = $this->getStepsArray(self::COMPANY_PROFILE_STEP);

        if (session()->exists('application_data')) {
            $application_data = session('application_data');
        }

        $company_name = $application_data['company_name'];

        return view('register.profile_company', compact('steps_array', 'company_name'));
    }

    public function saveCompanyProfileData(SaveCompanyProfileDataRequest $request)
    {
        $inputs = $request->validated();

        if(!empty($inputs)){

            $session_id = session()->getId();

            if(!empty($session_id)){
                $log_entry = $this->makeLogEntry([
                    'session_id' => $session_id,
                    'step_ended' => self::COMPANY_PROFILE_STEP
                ]);
            }

            if(empty($log_entry)){
                return json_encode(false);
            }

            $this->addToSessionApplicationData($inputs);

            $inputs['session_id'] = $session_id;
            //indian trick
            if($inputs['subscribe_to'] == true){
                $inputs['subscribe_to'] = 1;
            }else{
                $inputs['subscribe_to'] = 0;
            }

            $this->addOrCreateToApplicationTable($inputs);

            return json_encode(true);
        }
    }

    public function review()
    {
        $steps_array = $this->getStepsArray(self::REVIEW_STEP);

        $application_data = session('application_data');

        return view('register.review', compact(
                'steps_array',
                'application_data'
               ));
    }

    //this is the function where we make ip-call to x-road with our data to receive the document for that we are going to sign
    //sisning it is the worst part of the show
    public function reviewAndConfirmApplication()
    {
        $session_id = session()->getId();

        if(!empty($session_id)){
            $log_entry = $this->makeLogEntry([
                'session_id' => $session_id,
                'step_ended' => self::COMPANY_PROFILE_STEP
            ]);
        }

        if(empty($log_entry)){
            return json_encode(false);
        }

        //$result = $this->soapRequest->createEntryRequest(['session_id' => $session_id]);
        $result = $this->soapRequest->confirmEntryRequest([
            'partner_auth' => '60001019906',
            'entry_id' => '123456',
            'signed_document_ref' => 'data.bin'
        ]);

        return json_encode(true);
    }

    public function digital()
    {
        $steps_array = $this->getStepsArray(self::DIGITAL_STEP);

        return view('register.digital', compact('steps_array'));
    }

    public function signApplication(SignApplicationRequest $request)
    {
        $inputs = $request->validated();

        $session_id = session()->getId();
        if(!empty($session_id)){
                $log_entry = $this->makeLogEntry([
                    'session_id' => $session_id,
                    'step_ended' => self::DIGITAL_STEP
                ]);
            }

        if(empty($log_entry)){
            return json_encode(false);
        }

        $inputs['session_id'] = $session_id;

        $this->addOrCreateToApplicationTable($inputs);

        return json_encode(true);

    }

    public function payment()
    {
        $steps_array = $this->getStepsArray(self::PAYMENT_STEP);

        return view('register.payment', compact('steps_array'));
    }

    public function proceedPayment(ProceedPaymentRequest $request)
    {
        $inputs = $request->validated();

        $session_id = session()->getId();
        if(!empty($session_id)){
                $log_entry = $this->makeLogEntry([
                    'session_id' => $session_id,
                    'step_ended' => self::PAYMENT_STEP
                ]);
            }

        if(empty($log_entry)){
            return json_encode(false);
        }

        $params['session_id'] = $session_id;
        $params['payed'] = 1;

        $this->addToSessionApplicationData($params);

        return json_encode(true);
    }

    public function final()
    {
        $steps_array = $this->getStepsArray(self::FINAL_STEP);

        return view('register.final', compact('steps_array'));
    }

    protected function getStepsArray($active_number)
    {
        $steps_array = config('register.steps_array');
        $percent_array = config('register.percent_array');

        if($active_number < 8){
            $steps_array[$active_number]['status'] = 'active';
            $steps_array[$active_number]['percent'] = $percent_array[$active_number];
        }

        for ($i = 1; $i < $active_number; $i++){
            $steps_array[$i]['status'] = 'full';
        }

        return $steps_array;
    }

    protected function makeLogEntry($params)
    {
        $log_entry = Logger::where('session_id', '=', $params['session_id'] )->first();

        if(!empty($log_entry)){
                Logger::where('session_id', '=', $params['session_id'] )->update(['step_ended' => $params['step_ended']]);
                return true;
        }else{
            if ($params['step_ended'] == 1){
                Logger::create([
                    'session_id' => $params['session_id'],
                    'step_ended' => $params['step_ended']
                ]);

                return true;
            }
        }
        return false;
    }

    protected function addToSessionApplicationData($params)
    {
        if (session()->exists('application_data')) {
            $application_data = session('application_data');

            foreach($params as $key => $param){
                $application_data[$key] =  $param;
            }

            session(['application_data' => $application_data]);
        }
    }

    protected function addOrCreateToApplicationTable($params)
    {
        if(!empty($params)){
            $application = Application::where('session_id','=',$params['session_id'])->first();
            if(!empty($application)){
                $application_record = Application::where('session_id','=',$params['session_id'])->update($params);
            }else{
                $application_record = Application::create($params);
            }
        }
    }
}
