<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Logger;

class LoggerController extends Controller
{
    
    public function __construct()
    {
        
    }
    
    public function index(Request $request)
    {
        $counts = array();
        $period_conuts = 0;
        $steps_array = config('register.steps_array');
         
        if (!empty($request->start) && !empty($request->end)) {
            $start = Carbon::parse($request->start);
            $end = Carbon::parse($request->end);
            
            if($end->diffInDays($start) == 0){
                $end->addDays(1);
            }
            
            $logs = Logger::whereBetween('created_at', [$start, $end])
                    ->orderBy('created_at', 'DESC')->paginate(config('register.pagination'),[
                        'session_id',
                        'step_ended',
                        'created_at',
                        'updated_at'
                    ]);
            
            foreach($steps_array as $key => $step){
                $counts[$key] = Logger::whereBetween('created_at', [$start, $end])
                        ->where('step_ended','=', $key)->count();
                $period_conuts = $period_conuts + $counts[$key];
            }
        } else {
            $start='';
            $end = '';
            $logs = Logger::orderBy('created_at', 'DESC')->paginate(config('register.pagination'),[
                'session_id',
                'step_ended',
                'created_at',
                'updated_at'
            ]);
        }
        
       
        
        return view('admin.registration.index', compact(
                'logs',
                'steps_array',
                'start',
                'end',
                'counts',
                'period_conuts'
        ));
    }
    
    public function show()
    {
        
    }
    
}
