<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $fillable = [
        'created_at',
        'updated_at',
        'first_name',
        'last_name',
        'personal_id',
        'user_email',
        'session_id',
        'phone_number',
        'residence',
        'citizenship',
        'city',
        'street_address',
        'postcode',
        'company_code',
        'company_activity',
        'share_capital',
        'company_email',
        'company_phone_number',
        'company_address_estonias',
        'login_data',
        'digital_signature',
        'subscribe_to',
        'payed',
        'company_name',
        'state_provice'
    ];
}
