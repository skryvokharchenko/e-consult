<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logger extends Model
{
    public $table = 'loggers';
    
    protected $fillable = [
        'session_id',
        'step_ended',
        'created_at',
        'updated_at',
    ];
}
