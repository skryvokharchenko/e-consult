<?php
Route::get('/', function () {
    return redirect('/'. App\Http\Middleware\LocaleMiddleware::$mainLanguage. '/registration');
});


Route::group(['prefix' => App\Http\Middleware\LocaleMiddleware::getLocale(), 'middleware' => ['loc']], function(){
    
    Auth::routes(['register' => false]);
    
    Route::get('registration', 'RegistrationController@register')->name('registration');
    Route::get('application', 'RegistrationController@application')->name('application');
    Route::get('your_profile', 'RegistrationController@yourProfile')->name('your_profile');
    Route::get('company_profile', 'RegistrationController@companyProfile')->name('company_profile');
    Route::get('review', 'RegistrationController@review')->name('review');
    Route::get('digital', 'RegistrationController@digital')->name('digital');
    Route::get('payment', 'RegistrationController@payment')->name('payment');
    Route::get('final', 'RegistrationController@final')->name('final');
    
    
});

Route::post('/checkName', 'RegistrationController@checkName')->name('check.name');
Route::post('/loginViaMid', 'RegistrationController@loginViaMID')->name('login.via.chosen.mid');
Route::post('/loginViaSid', 'RegistrationController@loginViaSmartID')->name('login.via.chosen.sid');
Route::post('/saveUserProfileData', 'RegistrationController@saveUserProfileData')->name('save.user.profile');
Route::post('/saveCompanyProfileData', 'RegistrationController@saveCompanyProfileData')->name('save.company.profile');
Route::get('/reviewAndConfirm', 'RegistrationController@reviewAndConfirmApplication')->name('review.and.confirm');
Route::post('/applicationDigitalSign', 'RegistrationController@signApplication')->name('application.digital.sign');
Route::post('/proceedPayment', 'RegistrationController@proceedPayment')->name('proceed.payment');

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Currencies
    Route::delete('currencies/destroy', 'CurrencyController@massDestroy')->name('currencies.massDestroy');
    Route::resource('currencies', 'CurrencyController');

    // Transactiontypes
    Route::delete('transaction-types/destroy', 'TransactionTypeController@massDestroy')->name('transaction-types.massDestroy');
    Route::resource('transaction-types', 'TransactionTypeController');

    // Incomesources
    Route::delete('income-sources/destroy', 'IncomeSourceController@massDestroy')->name('income-sources.massDestroy');
    Route::resource('income-sources', 'IncomeSourceController');

    // Clientstatuses
    Route::delete('client-statuses/destroy', 'ClientStatusController@massDestroy')->name('client-statuses.massDestroy');
    Route::resource('client-statuses', 'ClientStatusController');

    // Projectstatuses
    Route::delete('project-statuses/destroy', 'ProjectStatusController@massDestroy')->name('project-statuses.massDestroy');
    Route::resource('project-statuses', 'ProjectStatusController');

    // Clients
    Route::delete('clients/destroy', 'ClientController@massDestroy')->name('clients.massDestroy');
    Route::resource('clients', 'ClientController');

    // Projects
    Route::delete('projects/destroy', 'ProjectController@massDestroy')->name('projects.massDestroy');
    Route::resource('projects', 'ProjectController');

    // Notes
    Route::delete('notes/destroy', 'NoteController@massDestroy')->name('notes.massDestroy');
    Route::resource('notes', 'NoteController');

    // Documents
    Route::delete('documents/destroy', 'DocumentController@massDestroy')->name('documents.massDestroy');
    Route::post('documents/media', 'DocumentController@storeMedia')->name('documents.storeMedia');
    Route::resource('documents', 'DocumentController');

    // Transactions
    Route::delete('transactions/destroy', 'TransactionController@massDestroy')->name('transactions.massDestroy');
    Route::resource('transactions', 'TransactionController');

    // Clientreports
    Route::delete('client-reports/destroy', 'ClientReportController@massDestroy')->name('client-reports.massDestroy');
    Route::resource('client-reports', 'ClientReportController');
    
    Route::resource('registration-controller', 'LoggerController')->only(['index', 'show']);
});


Route::get('setlocale/{lang}', function ($lang) {

    $referer = Redirect::back()->getTargetUrl(); 
    $parse_url = parse_url($referer, PHP_URL_PATH); 

    $segments = explode('/', $parse_url);

    if (in_array($segments[1], App\Http\Middleware\LocaleMiddleware::$languages)) {

        unset($segments[1]);
    } 
    
    array_splice($segments, 1, 0, $lang); 

    $url = Request::root().implode("/", $segments);
    
    if(parse_url($referer, PHP_URL_QUERY)){
        $url = $url.'?'. parse_url($referer, PHP_URL_QUERY);
    }
    return redirect($url);                 

})->name('setlocale');
