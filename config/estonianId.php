<?php

return [
    'OSCP_status_options' => [
            0 => "OCSP certificate status unknown",
            1 => "OCSP certificate status good / valid",
            2 => "OCSP internal error",
            3 => "OCSP certificate status revoked",
            4 => "Some error in script",
        ],
    'openssl_bin' => '/usr/bin/openssl',
    'tmp_dir' => '/var/tmp',
    // MID config variables start here
    'mid_timeout' => 20000,
    'mid_service_url' => 'https://tsp.demo.sk.ee/mid-api',
    'mid_service_uuid' => '00000000-0000-0000-0000-000000000000',
    'mid_service_name' => 'DEMO',
    'mid_auth_message' => 'Login to service',
    // SID config variables start here
    'sid_service_url' => 'https://sid.demo.sk.ee/smart-id-rp/v1/',
    'sid_service_uuid' => '00000000-0000-0000-0000-000000000000',
    'sid_service_name' => 'DEMO',
];

