<?php

return [
    'production_version' => true,
    'steps_array' => [
            1 => [
                'percent' => 0,
                'name' => 'register.name',
                'status' => ''
            ],
            2 => [
               'percent' => 0,
               'name' => 'register.fill_application',
               'status' => ''
            ],
            3 => [
               'percent' => 0,
               'name' => 'register.your_data',
               'status' => ''
            ],
            4 => [
               'percent' => 0,
               'name' => 'register.company_data',
               'status' => ''
            ],
            5 => [
               'percent' => 0,
               'name' => 'register.confirm',
               'status' => ''
            ],
            6 => [
               'percent' => 0,
               'name' => 'register.signature',
               'status' => ''
            ],
            7 => [
               'percent' => 0,
               'name' => 'register.payment',
               'status' => ''
            ]
        ],
    'percent_array' => [
        1 => 0,
        2 => 14,
        3 => 28,
        4 => 42,
        5 => 56,
        6 => 70,
        7 => 84,
        8 => 100
    ],
    'local_cert_name' => '',
    'pagination' => 50
];

