<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
	    $table->text('first_name');
            $table->text('last_name');
            $table->text('personal_id');
            $table->text('user_email');
            $table->text('session_id');
            $table->text('phone_number');
            $table->text('residence');
            $table->text('citizenship');
            $table->text('city');
            $table->text('street_address');
            $table->text('postcode');
            $table->text('company_code');
            $table->text('company_activity');
            $table->text('share_capital');
            $table->text('company_email');
            $table->text('company_phone_number');
            $table->text('company_address_estonias');
            $table->text('login_data');
            $table->text('digital_signature');
            $table->tinyInteger('subscribe_to');
            $table->tinyinteger('payed');
            $table->text('company_name');
            $table->text('state_province');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
