<?php
return [
    'name' => 'Название',
    'fill_application' => 'Заполнить заявку',
    'your_data' => 'Ваши данные',
    'company_data' => 'Данные фирмы',
    'confirm' => 'Подтвердить',
    'signature' => 'Подпись',
    'payment' => 'Оплата',
    'find_appropriate_company_name' => 'Найти подходящее название фирмы!',
    'company_name' => 'Название фирмы',
    'enter_appropriate_name' => 'Напишите подходящее название',
    'search' => 'Поиск',
    'remember_about_four' => 'При выборе названия помните о 4 пунктах',
    'unique' => 'Уникальное',
    'your_company_name_must_differ' => 'Название Вашей новой фирмы должно отличаться от зарегистрированных в Эстонии.',
    'not_used' => 'Не используется',
    'check_name_alone_plural_etc' => 'Проверьте название в единственном и множественном числе, вместе и раздельно, как часть другого названия.',
    'comfortable_for_user' => 'Удобное для пользователя',
    'name_must_be_latin_no_spec_chars' => 'Название должно быть латиницей, без символов и особых знаков.',
    'if_not_protected' => 'Не защищенное',
    'name_shod_differ_from registered_trade_marks' => 'Название должно отличаться от зарегистрированных в Эстонии торговых марок.',
    'login_for_enter' => 'Для заполнения зайдите',
    'application_to_commercial_register' => 'Заявление в коммерческий регистр',
    'id_card' => 'ID - Карта',
    'check_card_for_being_incerted' => 'Проверьте, чтобы ID-карта была вставлена в считыватель, нажмите кнопку ЗАЙТИ',
    'mobile_id' => 'Mobile - ID',
    'phone' => 'Телефон',
    'personal_code' => 'Личный код',
    'enter_btn' => 'Зайти',
    'smart_id' => 'Smart - ID',
    'go_back' => 'Назад',
    'please_login_to_proceed' => 'Для заполнения зайдите',
    'application_to_estonian_business' => 'Заявление в Эстонский бизнес регистр',
    'please_fill_in_your_data' => 'Пожалуйста, заполните ваши личные данные',
    'application_to_estonian_business_register' => 'Заявление в Эстонский бизнес регистр',
    'first_name' => 'Имя',
    'last_name' => 'Фамилия',
    'personal_id' => 'Персональный ID',
    'email_adress' => 'Email',
    'residence' => 'Residence',
    'citizenship' => 'Гражданство',
    'city' => 'Город',
    'street_adress' => 'Адрес',
    'state_province' => 'Штат/Провинция',
    'post_index' => 'Почтовый Индекс',
    'next_btn' => 'Далее',
    'please_fill_in' => 'Пожалуйста, заполните',
    'details' => 'детали',
    'code' => 'Код',
    'activity' => 'Деятельность',
    'share_capital' => 'Акционерный капитал',
    'company_email_addr' => 'Адрес электронной почты',
    'phone_number' => 'Номер телефона',
    'subscribe_to' => 'Подписаться на',
    'address_and_contact_person_service' => 'адрес и контактное лицо службы',
    'for_bucks_per_year' => 'за $ 179 в год',
    'estonian_office_adress' => 'Адрес офиса регистрации в Эстонии',
    'back_btn' => 'Назад',
    'please_revirew_you_application' => 'Пожалуйста, просмотрите вашу заявку',
    'new_company_details' => 'Детали новой компании',
    'company_email' => 'Электронная почта компании',
    'company_phone' => 'Телефон компании',
    'founder_and_board_member' => 'Основатель и член правления',
    'confirm' => 'Подтверждение',
    'sign_application' => 'Подать заявку',
    'please_sign_application' => 'Пожалуйста, подпишите заявку',
    'make_sure_your_card_is_in_reader' => 'Убедитесь, что ваша ID-карта правильно вставлена в ридер и нажмите «Подписать приложение»',
    'please_confirm_your_order' => 'Пожалуйста, подтвердите ваш заказ',
    'i_agree_to_unicount' => 'Я согласен на Unicount',
    'terms_of_service' => 'Условия Обслуживания',
    'privacy_policy' => 'Политика Конфиденциальности',
    'unicount_news_letter' => 'Unicount Newsletter',
    'cancel_order' => 'Отменить заказ',
    'your_application_submitted' => 'Ваша заявка отправлена',
    'proceed_button_text' => 'Продолжить',
    'name_available_text' => 'Название доступно. Нажмите продолжить',
    'name_unavailable_text' => 'Назва не доступно. Проверьте названия из списка и выберите другое.',
    'mid_errors' => [
        'phone_number_is_invalid' => 'The phone number you entered is invalid',
        'niid_is_invalid' => 'The national identity number you entered is invalid',
        'not_mid_client' => 'You are not a Mobile-ID client or your Mobile-ID certificates are revoked.',
        'invalid_mid_credentials' => 'Integration error with Mobile-ID. Invalid MID credentials',
        'mid_internal_error' => 'MID internal error',
        'wrong_session_id' => 'Wrong session id',
        'operation_canceled' => 'You cancelled operation from your phone.',
        'no_pin_in_phone' => 'You didn`t type in PIN code into your phone or there was a communication error.',
        'unable_to_reach_phone' => 'Unable to reach your phone. Please make sure your phone has mobile coverage.',
        'communication_error_phone' => 'Communication error. Unable to reach your phone.',
        'sim_card_conf_differs_from_provider' => 'MID configuration on SIM card differs from what`s configured on provider`s side.',
        'client_side_error_with_mid' => 'Client side error with mobile-ID integration. Error code:',
        'not_mid_client_or_revoked_cert' => 'You are not a Mobile-ID client or your Mobile-ID certificates are revoked.',
        'something_went_wrong' => 'Something went wrong with Mobile-ID service'
    ]
];