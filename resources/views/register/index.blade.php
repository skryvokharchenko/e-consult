@extends('layouts.main')
@section('content')
<div class="intro">
    <div class="container">
        <div class="intro__title title">{{ trans('register.find_appropriate_company_name') }}</div>
        <div id="checker" class="intro__subtitle subtitle">{{ trans('register.company_name')}}</div>
    </div>
     <div id = "status_spinner_name" class="d-flex justify-content-center" style="display:none;" >
            <span class="sr-only"><img width="50px" src="{{ asset('img/preloader.gif')}}"></img></span> 
     </div>
</div>
<div class="checkerform">
    <div class="checkerform__input">
        <div class="profile__input">
            <div class="input__title">{{ trans('register.enter_appropriate_name') }}</div>
            <form class="input-block">
                <input id="company-name" type="text" name="focus" required  placeholder="">
                <button class="close" type="reset">
            </form>
        </div>
    </div>
    <div class="checkerform__button">
        <a href="#" id="name-submit-btn"><button class="button" >{{ trans('register.search') }}</button></a>
    </div>
</div>
<div id="checker_out"></div>
<div class="inforow">
    <div class="inforow__container container">
        <div class="inforow__text">{{ trans('register.remember_about_four') }}</div>
            <div class="inforow__column">
                <div class="inforow-item">
                    <div class="inforow-item__icon">
                        <img src="{{ asset('img/info.png') }}" alt="">
                    </div>
                    <div class="inforow-item__title">{{ trans('register.unique') }}</div>
                        <div class="inforow-item__text">{{ trans('register.your_company_name_must_differ') }}</div>
                    </div>
            </div>		
            <div class="inforow__column">
                <div class="inforow-item">
                    <div class="inforow-item__icon">
                        <img src="{{ asset('img/info.png') }}" alt="">
                    </div>
                    <div class="inforow-item__title">{{ trans('register.not_used') }}</div>
                        <div class="inforow-item__text">{{ trans('register.check_name_alone_plural_etc') }}</div>
                    </div>
            </div>		
            <div class="inforow__column">
                <div class="inforow-item">
                    <div class="inforow-item__icon">
                        <img src="{{ asset('img/info.png') }}" alt="">
                    </div>
                    <div class="inforow-item__title">{{ trans('register.comfortable_for_user') }}</div>
                        <div class="inforow-item__text">{{ trans('register.name_must_be_latin_no_spec_chars') }}</div>
                    </div>
            </div>		
            <div class="inforow__column">
                <div class="inforow-item">
                    <div class="inforow-item__icon">
                        <img src="{{ asset('img/info.png') }}" alt="">
                    </div>
                    <div class="inforow-item__title">{{ trans('register.if_not_protected') }}</div>
                        <div class="inforow-item__text">{{ trans('register.name_shod_differ_from registered_trade_marks') }}</div>
                    </div>
            </div>		
    </div>
</div>
<div class="newsrow">
    <div class="newsrow__container container">
        <div class="newsrow__column">
            <div class="newsrow-item">
                <div class="newsrow-item__icon">
                    <img src="img/news.jpeg" alt="">
                </div>
                <div class="newsrow-item__title green__span">Business Brokering and Lending</div>
                <div class="newsrow-item__text">Business Brokering and Lending</div>
                <div class="newsrow-item__link">Tell Me More</div>
            </div>	
        </div>
        <div class="newsrow__column">
            <div class="newsrow-item">
                <div class="newsrow-item__icon">
                    <img src="{{ asset('img/news.jpeg') }}" alt="">
                </div>
                <div class="newsrow-item__title green__span">Business Brokering and Lending</div>
                <div class="newsrow-item__text">Business Brokering and Lending</div>
                <div class="newsrow-item__link">Tell Me More</div>
            </div>	
        </div>
        <div class="newsrow__column">
            <div class="newsrow-item">
                <div class="newsrow-item__icon">
                    <img src="{{ asset('img/news.jpeg') }}" alt="">
                </div>
                <div class="newsrow-item__title green__span">Business Brokering and Lending</div>
                <div class="newsrow-item__text">Business Brokering and Lending</div>
                <div class="newsrow-item__link">Tell Me More</div>
            </div>	
        </div>
    </div>
</div>
@endsection
