@extends('layouts.main')
@section('content')
<div class="intro">
		<div class="container">
			<div class="intro__title title"> {{ trans('register.please_fill_in_your_data') }}</div>
			<div class="intro__subtitle subtitle"> {{ trans('register.application_to_estonian_business_register') }}</div>
		</div>
	</div>

	<div class="profile">
		<div class="profile__items">
			<div class="profile__input">
				<div class="input__title"> {{ trans('register.first_name') }}</div>
				<form class="input-block">
					<input type="text" id="first_name" name="focus" required  placeholder="">
					<button class="close" type="reset">
				</form>
			</div>
			<div class="profile__input">
				<div class="input__title">{{ trans('register.last_name') }}</div>
				<form class="input-block">
					<input type="text" id="last_name" name="focus" required  placeholder="">
					<button class="close" type="reset">
				</form>
			</div>
			<div class="profile__input">
				<div class="input__title"> {{ trans('register.personal_id') }}</div>
				<form class="input-block">
					<input type="text" id="personal_id" name="focus" required  placeholder="">
					<button class="close" type="reset">
				</form>

			</div>
			<div class="profile__input large">
				<div class="input__title"> {{ trans('register.email_adress') }}</div>
				<form class="input-block">
					<input type="text" id="user_email" name="focus" required  placeholder="">
					<button class="close" type="reset">
				</form>
			</div>
			<div class="profile__input large">
				<div class="input__title">{{ trans('register.phone') }}</div>
				<form class="input-block">
					<input type="text" id="phone_number" name="focus" required  placeholder="">
					<button class="close" type="reset">
				</form>
			</div>
			<div class="profile__input">
				<div class="input__title"> {{ trans('register.residence') }}</div>
				<form class="input-block">
					<input type="text" id="residence" name="focus" required  placeholder="">
					<button class="close" type="reset">
				</form>
			</div>
			<div class="profile__input">
				<div class="input__title"> {{ trans('register.citizenship') }}</div>
				<form class="input-block">
					<input type="text" id="citizenship" name="focus" required  placeholder="">
					<button class="close" type="reset">
				</form>
			</div>
			<div class="profile__input">
				<div class="input__title"> {{ trans('register.city') }}</div>
				<form class="input-block">
					<input type="text" id="city" name="focus" required  placeholder="">
					<button class="close" type="reset">
				</form>
			</div>
			<div class="profile__input">
				<div class="input__title"> {{ trans('register.street_adress') }}</div>
				<form class="input-block">
					<input type="text" id="street_address" name="focus" required  placeholder="">
					<button class="close" type="reset">
				</form>
			</div>
			<div class="profile__input">
				<div class="input__title"> {{ trans('register.state_province') }}</div>
				<form class="input-block">
					<input type="text" id="state_province" name="focus" required  placeholder="">
					<button class="close" type="reset">
				</form>
			</div>
			<div class="profile__input">
				<div class="input__title"> {{ trans('register.post_index') }} </div>
				<form class="input-block">
					<input type="text" id="postcode" name="focus" required  placeholder="">
					<button class="close" type="reset">
				</form>
			</div>
		</div>
		<div class="profile__button">
			<a href="#" id="profile_submit_button"><button class="button"> {{ trans('register.next_btn') }}</button></a>
		</div>
	</div>

@endsection
