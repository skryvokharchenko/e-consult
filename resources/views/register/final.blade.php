@extends('layouts.main')
@section('content')
<div class="intro">
		<div class="container">
			<div class="intro__title title">{{ trans('register.your_application_submitted') }}</div>
			<div class="intro__subtitle subtitle"></div>
		</div>
	</div>

	<div class="finalrow">
			<div class="newsrow__container container">
				<div class="newsrow__column">
					<div class="newsrow-item">
						<div class="newsrow-item__icon">
							<img src="img/news.jpeg" alt="">
						</div>
						<div class="newsrow-item__title">Business Brokering and Lending</div>
						<div class="newsrow-item__text">Business Brokering and Lending</div>
						<div class="newsrow-item__link">Tell Me More</div>
					</div>	
				</div>
				<div class="newsrow__column">
					<div class="newsrow-item">
						<div class="newsrow-item__icon">
							<img src="img/news.jpeg" alt="">
						</div>
						<div class="newsrow-item__title">Business Brokering and Lending</div>
						<div class="newsrow-item__text">Business Brokering and Lending</div>
						<div class="newsrow-item__link">Tell Me More</div>
					</div>	
				</div>
			</div>
		</div>
@endsection
