@extends('layouts.main')
@section('content')
<div class="intro">
		<div class="container">
			<div class="intro__title title">{{ trans('register.please_confirm_your_order') }}</div>
			<div class="intro__subtitle subtitle">{{ trans('register.payment') }}</div>
		</div>
	</div>
	<div class="costlist">
		<div class="container">
			<div class="costlist__row">
				<div class="costlist__name">Name Name Name</div>
				<div class="costlist__price">Cost $</div>
			</div>
			<div class="costlist__row">
				<div class="costlist__name">Name</div>
				<div class="costlist__price">Cost $</div>
			</div>
			<div class="costlist__row">
				<div class="costlist__name">Name Name Name</div>
				<div class="costlist__price">Cost $</div>
			</div>
			<div class="row"></div>
			<div class="costlist__row">
				<div class="costlist__name"></div>
				<div class="costlist__price">Total Cost $</div>
			</div>

			<div class="profile__checker">
				<label class="checker__container">{{ trans('register.i_agree_to_unicount') }}<span class="green__span">{{ trans('register.terms_of_service') }}</span>
				  <input type="checkbox" id="terms_of_service_box">
				  <span class="checkmark"></span>
				</label>
			</div>
			<div class="profile__checker">
				<label class="checker__container">{{ trans('register.i_agree_to_unicount') }}<span class="green__span">{{ trans('register.privacy_policy') }}</span>
				  <input type="checkbox" id="privacy_policy_box">
				  <span class="checkmark"></span>
				</label>
			</div>
			<div class="profile__checker">
				<label class="checker__container">{{ trans('register.subscribe_to') }} <span class="green__span">{{ trans('register.unicount_news_letter') }}</span>
				  <input type="checkbox" id="news_letter_subscribe_box">
				  <span class="checkmark"></span>
				</label>
			</div>
			<div class="button-block">
				<div class="profile__button">
					<a href="#" class="proceed_payment_button"><button class="button">PayPal</button></a>
				</div>
				<div class="profile__button">
					<a href="#" class="proceed_payment_button"><button class="button">MasterCard</button></a>
				</div>
			</div>
		</div>
	</div>

	<div class="backlink">
		<a href="#"><span><</span>{{ trans('register.cancel_order') }}</a>
	</div>
@endsection

