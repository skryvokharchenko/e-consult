@extends('layouts.main')
@section('content')
<div class="intro">
		<div class="container">
			<div class="intro__title title">{{ trans('register.please_login_to_proceed') }} </div>
			<div id="error_message" class="intro__subtitle subtitle">{{ trans('register.application_to_estonian_business') }}</div>
		</div>
                 <div id = "status_spinner" class="d-flex justify-content-center" style="display:none;" >
                        <span class="sr-only"><img width="50px" src="{{ asset('img/preloader.gif')}}"></img></span>
                </div>
                <div id = "error_message" class="d-flex justify-content-center" ></div>

    </div>

	<div class="loginrow">
		<div class="loginrow__container container">
			<div class="loginrow__column">
				<div class="loginrow-item">
					<div class="loginrow-item__title">{{ trans('register.id_card') }}</div>
					<div class="loginrow-item__text">{{ trans('register.check_card_for_being_incerted') }}</div>
					<div class="loginrow-item__button">
						<a href="#" class="loginVia"><button class="button">{{ trans('register.enter_btn') }}</button></a>
					</div>
				</div>
			</div>
			<div class="loginrow__column">
				<div class="loginrow-item">
					<div class="loginrow-item__title"> {{ trans('register.mobile_id') }}</div>
					<div class="loginrow-item__text">
						<div class="profile__input">
							<div class="input__title"> {{ trans('register.phone') }}</div>
							<form class="input-block">
								<input id='mid_phone_number'type="text" name="focus" required  placeholder="">
								<button class="close" type="reset">
							</form>
						</div>
						<div class="profile__input">
							<div class="input__title"> {{ trans('register.personal_code') }}</div>
							<form class="input-block">
								<input id='mid_personal_code' type="text" name="focus" required  placeholder="">
								<button class="close" type="reset">
							</form>
						</div>
					</div>
					<div class="loginrow-item__button">
						<a href="#" id="loginViaMID"><button class="button">{{ trans('register.enter_btn') }}</button></a>
					</div>

				</div>
			</div>
			<div class="loginrow__column">
				<div class="loginrow-item">
					<div class="loginrow-item__title"> {{ trans('register.smart_id') }}</div>
					<div class="loginrow-item__text input">
						<div class="profile__input">
							<div class="input__title">{{ trans('register.personal_code') }}</div>
							<form class="input-block">
								<input id="personal_code" type="text" name="focus" required  placeholder="">
								<button class="close" type="reset">
							</form>
						</div>
					</div>
					<div class="loginrow-item__button">
						<a href="#" id="loginViaSID"><button class="button">{{ trans('register.enter_btn') }}</button></a>
					</div>
				</div>
			</div>
		</div>
		<div class="backlink">
			<a href="{{ route('registration') }}"><span><</span>Back</a>
		</div>
	</div>
@endsection



