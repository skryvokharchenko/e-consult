@extends('layouts.main')
@section('content')
<div class="content">
	<div class="intro">
		<div class="container">
			<div class="intro__title title">{{ trans('register.please_sign_application') }}</div>
			<div class="intro__subtitle subtitle">{{ trans('register.make_sure_your_card_is_in_reader') }}</div>
                        <input type="hidden" id="digital_signature" value="signed">
		</div>
	</div>
	<div class="checkerform">
		<div class="checkerform__button">
			<a href="#" id="sign_application" ><button class="button">{{ trans('register.sign_application') }}</button></a>
		</div>
	</div>

	<div class="backlink">
		<a href=""><span><</span>{{ trans('register.back_btn') }}</a>
	</div>
</div>
@endsection

