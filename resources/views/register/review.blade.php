@extends('layouts.main')
@section('content')
<div class="intro">
		<div class="container">
			<div class="intro__title title">{{ trans('register.please_revirew_you_application') }} </div>
			<div class="intro__subtitle subtitle">{{ trans('register.application_to_estonian_business_register') }}</div>
		</div>
	</div>

	<div class="review">
		<div class="review__items">
			<div class="review__title">{{ trans('register.new_company_details') }}: {{ $application_data['company_name'] }}</div>
			
			<div class="row"></div>

			<div class="code-block">
				<div class="title">{{ trans('register.code') }}</div>
				<div class="info">{{ $application_data['company_code'] }}</div>
			</div>
			<div class="activity-block">
				<div class="title">{{ trans('register.activity') }}</div>
				<div class="info">{{ $application_data['company_activity'] }}</div>
			</div>
			<div class="share-block">
				<div class="title">{{ trans('register.share_capital') }}</div>
				<div class="info">{{ $application_data['share_capital'] }}</div>
			</div>
			<div class="phone-block">
				<div class="title">{{ trans('register.company_phone') }}</div>
				<div class="info">{{ $application_data['company_phone_number'] }}</div>
			</div>
			<div class="email-block">
				<div class="title">{{ trans('register.company_email') }}</div>
				<div class="info">{{ $application_data['company_email'] }}</div>
			</div>
			<div class="regist-block">
				<div class="title">{{ trans('register.estonian_office_adress') }}</div>
				<div class="info">{{ $application_data['company_address_estonias'] }}</div>
			</div>

			<div class="row"></div>

			<div class="pers-block">
				<div class="title">{{ trans('register.founder_and_board_member') }}</div>
				<div class="info">{{ $application_data['first_name'] }} {{ $application_data['last_name'] }}</div>
				<div class="id-info">{{ $application_data['personal_id'] }}</div>
			</div>
			<div class="phone-block">
				<div class="title">{{ trans('register.phone') }}</div>
				<div class="info">{{ $application_data['phone_number'] }}</div>
			</div>
			<div class="email-block">
				<div class="title">{{ trans('register.email_adress') }}</div>
				<div class="info">{{ $application_data['user_email'] }}</div>
			</div>
			<div class="citiz-block">
				<div class="title">{{ trans('register.citizenship') }}</div>
				<div class="info">{{ $application_data['citizenship'] }}</div>
			</div>
			<div class="resid-block">
				<div class="title">{{ trans('register.residence') }}</div>
				<div class="info">{{ $application_data['residence'] }}</div>
			</div>

		</div>
		<div class="profile__button">
			<a href="#" id="confirm_review_button"><button class="button">{{ trans('register.confirm') }}</button></a>
		</div>
	</div>
	<div class="backlink">
		<a href=""><span><</span>{{ trans('register.back_btn') }}</a>
	</div>
@endsection

