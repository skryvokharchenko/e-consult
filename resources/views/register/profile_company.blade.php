@extends('layouts.main')
@section('content')
<div class="intro">
		<div class="container">
			<div class="intro__title title">{{ trans('register.please_fill_in') }} <span class="green__span">{{ $company_name }}</span> {{ trans('register.details') }} </div>
			<div class="intro__subtitle subtitle"> {{ trans('register.application_to_estonian_business_register') }}</div>
		</div>
	</div>

	<div class="profile">
		<div class="profile__items">
			<div class="profile__input">
				<div class="input__title">{{ trans('register.code') }}</div>
				<form class="input-block">
					<input type="text" id="company_code" name="focus" required  placeholder="">
					<button class="close" type="reset">
				</form>
			</div>
			<div class="profile__input">
				<div class="input__title">{{ trans('register.activity') }}</div>
				<form class="input-block">
					<input type="text" id="company_activity" name="focus" required  placeholder="">
					<button class="close" type="reset">
				</form>
			</div>
			<div class="profile__input">
				<div class="input__title">{{ trans('register.share_capital') }}</div>
				<form class="input-block">
					<input type="text" id="share_capital" name="focus" required  placeholder="">
					<button class="close" type="reset">
				</form>
			</div>
			<div class="profile__input large">
				<div class="input__title">{{ trans('register.company_email_addr') }}</div>
				<form class="input-block">
					<input type="text" id="company_email" name="focus" required  placeholder="">
					<button class="close" type="reset">
				</form>
			</div>
			<div class="profile__input large">
				<div class="input__title">{{ trans('register.phone_number') }}</div>
				<form class="input-block">
					<input type="text" id="company_phone_number" name="focus" required  placeholder="">
					<button class="close" type="reset">
				</form>
			</div>
			
			<div class="profile__checker">
				<label class="checker__container">{{ trans('register.subscribe_to') }} <span class="green__span">{{ trans('register.address_and_contact_person_service') }}</span> {{ trans('register.for_bucks_per_year') }}
				  <input id="subscribe_to" type="checkbox">
				  <span class="checkmark"></span>
				</label>
			</div>

			<div class="profile__input large">
				<div class="input__title">{{ trans('register.estonian_office_adress') }}</div>
				<form class="input-block">
					<input type="text" id="company_address_estonia" name="focus" required  placeholder="">
					<button class="close" type="reset">
				</form>
			</div>

		</div>
		<div class="profile__button">
			<a href="#" id="company_profile_submit_button"><button class="button">{{ trans('register.next_btn') }}</button></a>
		</div>
	</div>
	<div class="backlink">
		<a href=""><span><</span>{{ trans('register.back_btn') }}</a>
	</div>
@endsection

