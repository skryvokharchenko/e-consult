@extends('layouts.admin')
@section('styles')
<link href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css' rel="stylesheet"/>
@endsection
@section('content')
<div class="card">
    <div class="card-header">
        <div class="row">
           <h3 class="mb-4">Список попыток зарегестрировать компанию</h3>
        </div>
        <div class="row">
           <div class="col-5">
                                <h3 class="mb-4">Вывести заявки за период:</h3>
                            </div>
           <div class="col-7 text-right">
                                <div class="input-daterange datepicker row align-items-center">
                                    <div class="col-2 mb-4">
                                        {{ __('С даты') }}
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <div class="input-group input-group-alternative">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                                </div>
                                                <input class="form-control" id="start_date" placeholder="Start date" type="text"
                                                       value="{{(!empty($start)) ? $start : date('m/d/Y')}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-2 mb-4">
                                        {{ __('по дату') }}
                                        </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <div class="input-group input-group-alternative">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                                                </div>
                                                <input class="form-control" placeholder="End date" id="end_date" type="text"
                                                       value="{{!empty($end) ? $end : date('m/d/Y')}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="form-group">
                                            <button type="button" id="show_pullout"
                                                    class="btn btn-md btn-primary">{{__('Показать')}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
        </div>    
    </div>
    <div class="card-body">
        @if(!empty($counts))
        <div class='row'>
           <h4 class="mb-4"> Всего заявок за период: {{ $period_conuts }}</h4>
        </div> 
        <div class='row'>
           <h4 class="mb-4"> Количество заявок за период по этапам:</h4>
        </div>    
            <div class="table-responsive">
                <table class=" table table-bordered table-striped table-hover datatable datatable-Logger">
                    <thead>
                        <tr>
                            @foreach($steps_array as $step)
                                <th>
                                    Дошли до этапа "{{ trans($step['name']) }}"
                                </th>    
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            @foreach($counts as $count)
                                <td>
                                    {{ $count }}
                                </td>
                            @endforeach
                        </tr>
                    </tbody>
                </table>
            </div>
        @endif
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Logger">
                <thead>
                    <tr>  
                        <th>
                            Дата попытки
                        </th>
                        <th>
                            На каком этапе остановился
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($logs as $key => $entry)
                        <td>{{ date('m.d.Y', strtotime($entry->created_at)) }}</td>
                        <td>{{ trans($steps_array[$entry->step_ended]['name']) }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer ">
        <nav class="d-flex justify-content-end" aria-label="...">
            {{ !empty($logs) ? $logs->appends(['start' => $start, 'end' => $end])->links() : '' }}
        </nav>
    </div>
</div>    
@endsection
@section('scripts')
<script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js'></script>
<script>
    $('.datepicker').datepicker({
        calendarWeeks: true
    });
    
    $(document).on('click', '#show_pullout', function (e) {
        var start = $('#start_date').val();
        var end = $('#end_date').val();
        window.location = '?start=' + start + '&end='+end;
    });
    
</script>
@endsection


