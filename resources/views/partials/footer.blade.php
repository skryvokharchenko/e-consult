@section('scripts')
<script>
    //ajax routes
    var check_name_url = '{{ route('check.name') }}';
    var login_via_mid_url = '{{ route('login.via.chosen.mid') }}';
    var login_via_sid_url = '{{ route('login.via.chosen.sid') }}';
    var save_user_profile_url = '{{ route('save.user.profile') }}';
    var save_company_profile_url = '{{ route('save.company.profile') }}';
    var review_confirm_url = '{{ route('review.and.confirm') }}';
    var application_digital_sign_url = '{{ route('application.digital.sign') }}';
    var proceed_payment_url = '{{ route('proceed.payment') }}';
    //redirect routes
    var registration_url = '{{ route('registration') }}';
    var application_redirect_url = '{{ route('application') }}'
    var user_profile_redirect_url = '{{ route('your_profile') }}';
    var company_profile_redirect_url = '{{ route('company_profile') }}';
    var review_redirect_url = '{{ route('review') }}';
    var digital_redirect_url = '{{ route('digital') }}';
    var payment_redirect_url = '{{ route('payment') }}';
    var final_page_redirect_url = '{{ route('final') }}';
    var proceed_button_text = '{{ trans('register.proceed_button_text')}}';
    var name_available_text = '{{ trans('register.name_available_text')}}';
    var name_unavailable_text = '{{trans('register.name_unavailable_text')}}';
</script>
<script src="{{ asset('js/front.js') }}"></script>
@endsection
<div class="footer">
    <div class="container">
        <div class="logo">
            <a href="index.html"><img src="{{ asset('img/LOGO.png') }}" alt=""></a>
        </div>
        <div class="footer-info">
            <a href="#">FAQ</a>
            <a href="#">Terms Of Use</a>
            <a href="#">Privacy Policy</a>
        </div>
        <div class="footer-network">
            <div class="foot-text"></div>
            <div class="foot-netw">
                <a href=""><span class="insta"></span></a>
                <a href=""><span class="faceb"></span></a>
                <a href=""><span class="twit"></span></a>
                <a href=""><span class="telega"></span></a>
            </div>
        </div>
    </div>
</div>

