<div class="header">
    <div class="header__container container">
	<div class="header__logo">
            <a href="index.html"><img src="{{ asset('img/LOGO.png') }}" alt=""></a>
	</div>
	<div class="header__statusbar">
            <div class="status__row"></div>
            <div class="statusbar__items">
		<ul>
                    @foreach($steps_array as $step)
                        <li class="status__item {{ $step['status'] }}"><span class="procent">{{ !empty($step['percent']) ? $step['percent'].'%' : '' }}</span>{{ trans($step['name']) }}</li>
                    @endforeach	
		</ul>
            </div>
	</div>
    </div>
</div>