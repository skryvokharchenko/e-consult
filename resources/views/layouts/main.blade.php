<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Site</title>
        
	<link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@300&display=swap" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300&display=swap" rel="stylesheet"> 
	<link href="{{ asset('css/style.css') }}" rel="stylesheet">
	<link href="{{ asset('css/media.css') }}" rel="stylesheet">
	@yield('styles')
	
</head>
<body>
    @include('partials.header')
    @yield('content')
    @include('partials.footer')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    @yield('scripts')
</body>