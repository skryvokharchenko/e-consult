jQuery(document).ready(function ($) { 
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $(".profile__input").on('click', function() {                   
            this.querySelector('input').focus();
            var temp = this.querySelector('.input__title');
            temp.style.top = "0px";
    });
    
    if(window.screen.width < 426) {
        var temp = document.querySelector('.status__item.active span');
        temp.style.background = "linear-gradient(#74ff0000 100%, #00e685 0%)";
    }    

    $("#name-submit-btn").on('click', function(event){
        event.preventDefault();
        
        if ($('#company-name').val().length === 0) {
            $('#company-name').parent().parent().click();
            return false;
        }
        
        var company_name = $('#company-name').val();
        
        var formData = new FormData();
        formData.append('company_name', company_name);
        
        $('#status_spinner_name').toggle();
        $.ajax({
            type: 'post',
            url: check_name_url,
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                console.log(data);
                $('#status_spinner_name').toggle();
                if(data === 'Ok'){
                    $('#checker_out').replaceWith('<div id="checker_out" class="container"></div>');
                    $('#checker_out').append('<p class="center">'+name_available_text+'</p>');          
                    $("#name-submit-btn").replaceWith('<a href="#" id="go-to-application"><button class="button" >'+proceed_button_text+'</button></a>');
                }else{
                    $('#checker_out').replaceWith('<div id="checker_out" class="container"></div>');
                    $('#checker_out').append('<p class="center">'+name_unavailable_text+'</p>');
                    $('#checker_out').append('<ul id="result_list"></ul>');
                    
                    $.each(data, function (index, value) {
                        $('#result_list').append('<li>'+value+'</li>');
                    });
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            },
             dataType: "json"
        });
    });
    
    
    jQuery(document).on('click', '#go-to-application', function(event){
        event.preventDefault();
        
        window.location.assign(application_redirect_url);
    });
    
    $('#loginViaMID').on('click', function(event){
        event.preventDefault();
        
        if ($('#mid_phone_number').val().length === 0) {
            return false;
        }
        
        if ($('#mid_personal_code').val().length === 0) {
            return false;
        }
        
        var mid_phone_number = $('#mid_phone_number').val();
        var mid_personal_code = $('#mid_personal_code').val();
        
        var formData = new FormData();
        formData.append('mid_phone_number', mid_phone_number);
        formData.append('mid_personal_code', mid_personal_code);
        
        
        $('#status_spinner').toggle();
        $('#error_message').text('');
       
        $.ajax({
            type: 'post',
            url: login_via_mid_url,
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                $('#status_spinner').toggle();
                
                if(data['status'] == 'Ok'){
                    window.location.assign(user_profile_redirect_url);
                }else if (data['status'] == 'session_fault'){
                    window.location.assign(registration_url);
                }else{
                   $('#error_message').text(data['message']);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            },
            dataType: "json"
        });
    });
    
    $('#loginViaSID').on('click', function(event){
        event.preventDefault();
        
        if($('#personal_code').val().length === 0){
            return false;
        }
        
        var personal_code = $('#personal_code').val();
        
        var formData = new FormData();
        formData.append('personal_code', personal_code);
        
        $('#status_spinner').toggle();
        $('#error_message').text('');
        
        $.ajax({
            type: 'post',
            url: login_via_sid_url,
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                $('#status_spinner').toggle();
                
                if(data['status'] == 'Ok'){
                    window.location.assign(user_profile_redirect_url);
                }else if (data['status'] == 'session_fault'){
                    window.location.assign(registration_url);
                }else{
                   $('#error_message').text(data['message']);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            },
            dataType: "json"
        });
    });
    
    $('#profile_submit_button').on('click', function(event){
        event.preventDefault();
        
        var invalid = false;
        
        $('input[type=text]').each(function(){
            if($(this).val().length == 0){
                $(this).parent().parent().click();
                invalid = true;
                return false;
            }
        });
        
        if(invalid){
            return false;
        } 
        
        var first_name = $('#first_name').val();
        var last_name = $('#last_name').val();
        var personal_id = $('#personal_id').val();
        var email = $('#user_email').val();
        var phone_number = $('#phone_number').val();
        var residence = $('#residence').val();
        var citizenship = $('#citizenship').val();
        var city = $('#city').val();
        var street_address = $('#street_address').val();
        var state_province = $('#state_province').val();
        var postcode = $('#postcode').val();
        
        var formData = new FormData();
        formData.append('first_name', first_name);
        formData.append('last_name', last_name);
        formData.append('personal_id', personal_id);
        formData.append('user_email', email);
        formData.append('phone_number', phone_number);
        formData.append('residence', residence);
        formData.append('citizenship', citizenship);
        formData.append('city', city);
        formData.append('street_address', street_address);
        formData.append('state_province', state_province);
        formData.append('postcode', postcode);
        
        $.ajax({
            type: 'post',
            url: save_user_profile_url,
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                if (Boolean(data) != false){
                    window.location.assign(company_profile_redirect_url);
                }else{
                    window.location.assign(registration_url);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
    
    $('#company_profile_submit_button').on('click', function(event){
        event.preventDefault();
        
        var invalid = false;
        
        $('input[type=text]').each(function(){
            if($(this).val().length == 0){
                $(this).parent().parent().click();
                invalid = true;
                return false;
            }
        });
        
        if(invalid){
            return false;
        } 
        
        var company_code = $('#company_code').val();
        var company_activity = $('#company_activity').val();
        var share_capital = $('#share_capital').val();
        var company_email = $('#company_email').val();
        var company_phone_number = $('#company_phone_number').val();
        var subscribe_to = $('#subscribe_to').is(':checked');
        var company_address_estonia = $('#company_address_estonia').val();
        
        formData = new FormData();
        formData.append('company_code', company_code);
        formData.append('company_activity', company_activity);
        formData.append('share_capital', share_capital);
        formData.append('company_email', company_email);
        formData.append('company_phone_number', company_phone_number);
        formData.append('subscribe_to', Boolean(subscribe_to));
        formData.append('company_address_estonias', company_address_estonia);
       
        $.ajax({
            type: 'post',
            url: save_company_profile_url,
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                if (Boolean(data) != false){
                    window.location.assign(review_redirect_url);
                }else{
                    window.location.assign(registration_url);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
    
    $('#confirm_review_button').on('click', function(event){
        event.preventDefault();
        
        $.ajax({
            url: review_confirm_url,
            type: 'GET',
            dataType: 'json',
            success: function(data) {
                console.log(data);
                //if(Boolean(data) !=false){
                //    window.location.assign(digital_redirect_url);
                //}else{
                //    window.location.assign(registration_url);
               // }
            }
        });
    });
    
    $('#sign_application').on('click', function(event){
        event.preventDefault();
        
        var digital_signature = $('#digital_signature').val();
        
        var formData = new FormData();
        formData.append('digital_signature', digital_signature);
        
        $.ajax({
            type: 'post',
            url: application_digital_sign_url,
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                if (Boolean(data) != false){
                    window.location.assign(payment_redirect_url);
                }else{
                    window.location.assign(registration_url);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
    
    $('.proceed_payment_button').on('click', function(event){
        event.preventDefault();
        
        var terms_of_service = $('#terms_of_service_box').is(':checked');
        var privacy_policy = $('#privacy_policy_box').is(':checked');
        var news_letter_subscribe = $('#subscribe_to').is(':checked');
        
        if(terms_of_service == false){ return false;}
        if(privacy_policy == false){return false;}
        
        var formData = new FormData();
        formData.append('terms_of_service', terms_of_service);
        formData.append('privacy_policy', privacy_policy);
        formData.append('news_letter_subscribe', news_letter_subscribe);
        
        $.ajax({
            type: 'post',
            url: proceed_payment_url,
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                if (Boolean(data) != false){
                    window.location.assign(final_page_redirect_url);
                }else{
                    window.location.assign(registration_url);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });
    });
});